Carriage distribution breakout PCB 
----------------------------------
Designed for Series 1 2014 Model, using geda:gaf 

This distribution pcb connects signals from the gantry pcb distirbution board,
and connects the hot ends, fans, thermistors, aux, and extruder motors.

Refer to semver.org for versioning information. As far as I know, symantic
versioning hasn't been used for HW before, dependancies will be added as they
become clear in the master overview of the systems integrated with this board. 


but my symobls are missing from my schematic!!
----------------------------------------------
i'm using git submodules to make a common symbol and footprint library available
for the projects.  follow the instructions below in 'using submodules' and see
if that helps.  if it does not, then file an issue
 
that way the project can checkout the latest symbols and footprints, or check
out specific feature branches (large pads for easy soldering, for instance)
  
or a project could be verified to work with a specific commit in the library,
fixing the symbols and footprints used at a certain commit so there's no chance
they change for a given project

additional private libraries of symbols and footprints can be added as static
parts of the repo (use gafrc to add paths for gschem to search for when working
with schematics, and add paths to pcb on on the commandline or in the gui)

it is still a work in progress and will probably change

using submodules
----------------
clone this repo, enter the directory, and run

git submodule update --init

or the equivalent command in your git gui tool

when checking out dev branches or earlier tags of the project, you will have to
update the submodules to get the correct version of parts (symbols and
footprints) used during development. The git submodule update --init command
should also be used after checking out these earlier versions.

Using schdiff with git's difftool
---------------------------------
schdiff allows the user to compare schematics from different versions.

example showing a diff from the current HEAD to 30 commits back:
git difftool -x schdiff HEAD~30 carriagebreakout.sch

