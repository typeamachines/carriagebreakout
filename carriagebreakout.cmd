# Pin name action command file

# Start of element CONN10
ChangePinName(CONN10, 20, fan0)
ChangePinName(CONN10, 19, fan0_1)
ChangePinName(CONN10, 18, aux0-a)
ChangePinName(CONN10, 17, therm1)
ChangePinName(CONN10, 16, therm0)
ChangePinName(CONN10, 15, gnd)
ChangePinName(CONN10, 14, fan1_1)
ChangePinName(CONN10, 13, aux0-b)
ChangePinName(CONN10, 12, fan1)
ChangePinName(CONN10, 11, 5v_vcc)
ChangePinName(CONN10, 10, gnd)
ChangePinName(CONN10, 9, E1M_1B)
ChangePinName(CONN10, 8, E0M_1A)
ChangePinName(CONN10, 7, E1M_2A)
ChangePinName(CONN10, 6, E0M_2B)
ChangePinName(CONN10, 5, gnd)
ChangePinName(CONN10, 4, E0M_2A)
ChangePinName(CONN10, 3, E1M_1A)
ChangePinName(CONN10, 2, E0M_1B)
ChangePinName(CONN10, 1, E1M_2B)

# Start of element CONN9
ChangePinName(CONN9, 6, gnd)
ChangePinName(CONN9, 5, 24Vdc)
ChangePinName(CONN9, 4, HeatRtn)
ChangePinName(CONN9, 3, Heat1)
ChangePinName(CONN9, 2, HeatRtn)
ChangePinName(CONN9, 1, Heat0)

# Start of element CONN8
ChangePinName(CONN8, 4, 4)
ChangePinName(CONN8, 3, 3)
ChangePinName(CONN8, 2, 2)
ChangePinName(CONN8, 1, 1)

# Start of element CONN7
ChangePinName(CONN7, 4, 4)
ChangePinName(CONN7, 3, 3)
ChangePinName(CONN7, 2, 2)
ChangePinName(CONN7, 1, 1)

# Start of element CONN6
ChangePinName(CONN6, 2, 2)
ChangePinName(CONN6, 1, 1)

# Start of element CONN5
ChangePinName(CONN5, 2, 2)
ChangePinName(CONN5, 1, 1)

# Start of element CONN4
ChangePinName(CONN4, 4, 4)
ChangePinName(CONN4, 3, 3)
ChangePinName(CONN4, 2, 2)
ChangePinName(CONN4, 1, 1)

# Start of element CONN3
ChangePinName(CONN3, 4, 4)
ChangePinName(CONN3, 3, 3)
ChangePinName(CONN3, 2, 2)
ChangePinName(CONN3, 1, 1)

# Start of element CONN2
ChangePinName(CONN2, 4, 4)
ChangePinName(CONN2, 3, 3)
ChangePinName(CONN2, 2, 2)
ChangePinName(CONN2, 1, 1)

# Start of element CONN1
ChangePinName(CONN1, 2, 2)
ChangePinName(CONN1, 1, 1)
